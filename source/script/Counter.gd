extends Label


var increase: bool = false
var count: int = 0

var green: Color = Color(0.0 / 255.0, 175.0 / 255.0, 0.0 / 255.0)
var red: Color = Color(129.0 / 255.0, 25.0 / 255.0, 15.0 / 255.0)

var regex = RegEx.new()


# BUILTINS - - - - - - - - -


func _ready() -> void:
	regex.compile("(\\d)(?=(\\d\\d\\d)+([^\\d]|$))")
	var _t: int
	var pos: Vector2 = rect_position
	var new_pos: Vector2
	var new_pos_x: float = rand_range(pos.x - 20.0, pos.x + 20.0)
	if increase:
		self.text = "$ +%s" % format_number(count)
		self.set("custom_colors/font_color", green)
		new_pos = Vector2(new_pos_x, rand_range(pos.y - 150.0, pos.y - 100.0))
		($CPUParticles2D as CPUParticles2D).emitting = true
	else:
		self.text = "$ -%s" % format_number(count)
		self.set("custom_colors/font_color", red)
		new_pos = Vector2(new_pos_x, rand_range(pos.y + 100.0, pos.y + 150.0))
	_t = ($Tween as Tween).interpolate_property(self, "rect_position", pos, new_pos, 1.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	yield(get_tree().create_timer(1.2), "timeout")
	_t = ($Tween as Tween).interpolate_property(self, "modulate:a", 1.0, 0.0, 0.3)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	yield(get_tree().create_timer(0.3), "timeout")
	queue_free()


# METHODS - - - - - - - - -


# форматирование числа, проставление знаков между порядками
func format_number(number: float) -> String:
	var formated_number: String = regex.sub(String(number), "$1,", true)
	return formated_number


# SIGNALS - - - - - - - - -
